<?php

/**
 * Implements hook_alpha_process_region()
 */
function subtle_simplicity_alpha_process_region(&$vars) {
  if (in_array($vars['elements']['#region'], array('user_second'))) {
    $theme = alpha_get_theme();
    if ($theme->page['secondary_menu']) {
      $vars['secondary_menu'] = $theme->page['secondary_menu'];
    }
    else {
      $links = array();
      $links[] = array(
          'href' => 'user',
          'title' => 'Login',
      );
      if (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL)) {
        $links[] = array(
          'href' => 'user/register',
          'title' => 'Register',
        );
        $links[] = array(
          'href' => 'user/password',
          'title' => 'Reset password',
        );
      };
      $vars['secondary_menu'] = $links;
    }
  }
}

