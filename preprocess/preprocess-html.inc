<?php

/**
 * Implements hook_alpha_preprocess_html()
 */
function subtle_simplicity_alpha_preprocess_html(&$vars) {
  drupal_add_css('http://fonts.googleapis.com/css?family=Fredericka the Great:regular|Questrial:400|Inconsolata:400&amp;subset=latin', array('type' => 'external'));
}

